#!/bin/bash

CWD=$(dirname $0)

set -a
. $CWD/../.env
set +a

mkdir -p $CWD/../.lyra

docker build \
	--build-arg LYRA_VERSION \
	--build-arg LYRA_NETWORK \
	--build-arg LYRA_LEXUSER_PASS \
	--build-arg LYRA_WALLET \
	-t lyra/mongo -f $CWD/../mongo.Dockerfile $CWD/..

docker build \
	--build-arg LYRA_VERSION \
	--build-arg LYRA_NETWORK \
	--build-arg LYRA_LEXUSER_PASS \
	--build-arg LYRA_WALLET \
	-t lyra -f $CWD/../lyra.Dockerfile $CWD/..
